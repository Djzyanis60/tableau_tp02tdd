﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableau_TP02
{
    public class Program
    {
        public static int saisie()
        {
            int[] tab_valeur = new int[10];
            int valeur_saisie, indice_valeur_research;
            string valeur_saisie_string;
            Console.WriteLine("Saisir dix valeurs :");
            for (indice_valeur_research = 0; indice_valeur_research < 10; indice_valeur_research++)
            {
                Console.Write("{0} ère valeur : ", indice_valeur_research + 1);
                valeur_saisie_string = Console.ReadLine();
                int.TryParse(valeur_saisie_string, out valeur_saisie);
                tab_valeur[indice_valeur_research] = valeur_saisie;
            }
            return tab_valeur[indice_valeur_research];
        }
        static void Main(string[] args)
        {
            
        }
    }
}
